/*

Big ass LED display driver - (c) 2018 Frederic Pasteleurs <frederic@askarel.be>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

  To compile, use the board 'D1 Mini Pro' in Arduino

*/
// Some constants: define the pins connected to the LED display arrays.
// Since the two lines are completely independent and have identical hardware, 
// the pins OE, ST and CP are shared between the two displays: the ESP will drive both at the same time
// Only the data pins are display specific.

#define PIN_OE  D8
#define PIN_ST  D7
#define PIN_D   D6
#define PIN_CP  D5
#define PIN_D2  D0

// Framebuffer length, in pixels
#define LINELENGTH  150
// Amount of display lines
#define LINES       2
// The frame buffer: Each byte of the frame buffer is a column
unsigned char framebuffers[LINES][LINELENGTH];

// ESP8266 - OS timer
extern "C" {
#include "user_interface.h"
}

// Include EEPROM library, to store parameters like system password, SSID and WPA key
#include <EEPROM.h>

// The screen redraw routine is handled by a timer interrupt
volatile int dispy;
os_timer_t MyTimer;

// A basic font
const char dm_charset[255][5] = {
// Bottom...Top  
  { 0, 0, 0, 0, 0 }, // 0x00 - NUL
  { 0b00111110, 0b01010101, 0b01110001, 0b01010101, 0b00111110 }, // 0x01
  { 0, 0, 0, 0, 0 }, // 0x02
  { 0, 0, 0, 0, 0 }, // 0x03
  { 0, 0, 0, 0, 0 }, // 0x04
  { 0, 0, 0, 0, 0 }, // 0x05
  { 0, 0, 0, 0, 0 }, // 0x06
  { 0, 0, 0, 0, 0 }, // 0x07
  { 0, 0, 0, 0, 0 }, // 0x08
  { 0, 0, 0, 0, 0 }, // 0x09
  { 0, 0, 0, 0, 0 }, // 0x0a
  { 0, 0, 0, 0, 0 }, // 0x0b
  { 0, 0, 0, 0, 0 }, // 0x0c
  { 0, 0, 0, 0, 0 }, // 0x0d
  { 0, 0, 0, 0, 0 }, // 0x0e
  { 0, 0, 0, 0, 0 }, // 0x0f
  { 0, 0, 0, 0, 0 }, // 0x10
  { 0, 0, 0, 0, 0 }, // 0x11
  { 0, 0, 0, 0, 0 }, // 0x12
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 },                   // 0x20 - space
  { 0, 0, 0b01011111, 0, 0 },          // Ox21 - !
  { 0, 0b00000011, 0, 0b00000011, 0 }, // 0x22 - "
  { 0b00010100, 0b01111111, 0b00010100, 0b01111111, 0b00010100 }, // 0x23 - #
  { 0, 0b00001010, 0b01111111, 0b01010000, 0 }, // 0x24 - $
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0b01000000, 0, 0 }, // 0x2e - .
  { 0b01000000, 0b00110000, 0b00001000, 0b00000110, 0b00000001 }, // 0x2f - /
  { 0b00111110, 0b01010001, 0b01001001, 0b01000101, 0b00111110 }, // 0x30 - 0
  { 0, 0, 0, 0, 0 }, // 0x31 - 1
  { 0, 0, 0, 0, 0 }, // 0x32 - 2
  { 0, 0, 0, 0, 0 }, // 0x33 - 3
  { 0, 0, 0, 0, 0 }, // 0X34 - 4
  { 0, 0, 0, 0, 0 }, // 0x35 - 5
  { 0, 0, 0, 0, 0 }, // 0x36 - 6
  { 0, 0, 0, 0, 0 }, // 0x37 - 7
  { 0, 0, 0, 0, 0 }, // 0x38 - 8 
  { 0, 0, 0, 0, 0 }, // 0x39 - 9
  { 0, 0, 0, 0, 0 }, // 0x3A - :
  { 0, 0, 0, 0, 0 }, // 0x3B - ;
  { 0, 0, 0, 0, 0 }, // 0x3c - <
  { 0, 0, 0, 0, 0 }, // 0x3d - =
  { 0, 0b01000001, 0b00100010, 0b00010100, 0b00001000 }, // 0x3e - >
  { 0, 0, 0, 0, 0 }, // 0x3f - ?
  { 0, 0, 0, 0, 0 }, // 0x40 - @
  { 0b01111110, 0b00001001, 0b00001001, 0b00001001, 0b01111110 }, // 0x41 - A
  { 0b01111111, 0b01001001, 0b01001001, 0b01001001, 0b00110110 }, // 0x42 - B
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x47 - G
  { 0b01111111, 0b00001000, 0b00001000, 0b00001000, 0b01111111 }, // 0x48 - H
  { 0, 0, 0, 0, 0 }, // 0x49 - I
  { 0, 0, 0, 0, 0 }, // 0x4A - J
  { 0, 0, 0, 0, 0 }, 
  { 0b01111111, 0b01000000, 0b01000000, 0b01000000, 0b01000000 }, // 0x4C - L
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0b01111111, 0b00001001, 0b00001001, 0b00001001, 0b00000110 }, // 0x50 - P
  { 0, 0, 0, 0, 0 }, 
  { 0b01111111, 0b00001001, 0b00011001, 0b00101001, 0b01000110 }, // 0x52 - R
  { 0b01000110, 0b01001001, 0b01001001, 0b01001001, 0b00110000 }, // 0x53 - S
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0b01100011, 0b00010100, 0b00001000, 0b00010100, 0b01100011 }, // 0x58 - X
  { 0, 0, 0, 0, 0 }, // 0x59 - Y
  { 0, 0, 0, 0, 0 }, // 0x5A - Z
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x5F - _
  { 0, 0, 0, 0, 0 }, // 0x60 - 
  { 0b00100100, 0b1010100, 0b01010100, 0b01010100, 0b00101000 }, // 0x61 - a
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x64 - d
  { 0b00111000, 0b01010100, 0b01010100, 0b01010100, 0b01001000 }, // 0x65 - e
  { 0, 0, 0, 0, 0 }, 
  { 0b01001100, 0b01010010, 0b01010010, 0b01010010, 0b00111100 }, // 0x67 - g
  { 0b01111110, 0b00010000, 0b00001000, 0b00001000, 0b01110000 }, // 0x68 - h
  { 0, 0, 0b1111010, 0, 0 }, // 0x69 - i
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x71 - k
  { 0, 0b00111110, 0b01000000, 0b01000000, 0 }, // 0x72 - l
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0b00111000, 0b01000100, 0b01000100, 0b01000100, 0b00111000 }, // 0x69 - o
  { 0, 0, 0, 0, 0 }, // 0x70 - p
  { 0, 0, 0, 0, 0 }, // 0x71 - q
  { 0b01111100, 0b00010000, 0b00001000, 0, 0 }, // 0x72 - r
  { 0b01001000, 0b01010100, 0b01010100, 0b01010100, 0b00100000 }, // 0x73 - s
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0b00011100, 0b00100000, 0b01000000, 0b00100000, 0b00011100 }, // 0x76 - v
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x79 - y
  { 0, 0, 0, 0, 0 }, // 0x7a - z
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0b01111111, 0, 0 }, // 0x7c - |
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, // 0x7f
  { 0, 0, 0, 0, 0 }, // 0x80 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 }, 
  { 0, 0, 0, 0, 0 } // 0xff
};


void setup () 
{
  int i;
  pinMode (PIN_OE, OUTPUT);
  pinMode (PIN_ST, OUTPUT);
  pinMode (PIN_D, OUTPUT);
  pinMode (PIN_D2, OUTPUT);
  pinMode (PIN_CP, OUTPUT);
  digitalWrite (PIN_ST, HIGH);
  digitalWrite (PIN_OE, LOW);
  digitalWrite (PIN_D, LOW);
  digitalWrite (PIN_D2, LOW);
  digitalWrite (PIN_CP, LOW);
  dispy=0;

  os_timer_setfn(&MyTimer, TimerInterruptCallback, NULL);
  os_timer_arm(&MyTimer, 3, true);

  Serial.begin(115200);
 for (i=0; i < LINELENGTH; i++)
  {
    framebuffers[0][i]=0x00;
    framebuffers[1][i]=0x00;
  }
 for (i=0; i < 5; i++)
  {
    framebuffers[1][12+i]=dm_charset['H'][i];
    framebuffers[1][18+i]=dm_charset['a'][i];
    framebuffers[1][24+i]=dm_charset['s'][i];
    framebuffers[1][30+i]=dm_charset['h'][i];
    framebuffers[1][36+i]=dm_charset['#'][i];
    framebuffers[1][42+i]=dm_charset['H'][i];
    framebuffers[1][48+i]=dm_charset['a'][i];
    framebuffers[1][54+i]=dm_charset['s'][i];
    framebuffers[1][60+i]=dm_charset['h'][i];
    framebuffers[1][66+i]=dm_charset['/'][i];
    framebuffers[1][72+i]=dm_charset['/'][i];
    framebuffers[1][78+i]=dm_charset['A'][i];
    framebuffers[1][84+i]=dm_charset['l'][i];
    framebuffers[1][90+i]=dm_charset['g'][i];
    framebuffers[1][96+i]=dm_charset['o'][i];
    framebuffers[1][102+i]=dm_charset['R'][i];
    framebuffers[1][108+i]=dm_charset['a'][i];
    framebuffers[1][114+i]=dm_charset['v'][i];
    framebuffers[1][120+i]=dm_charset['e'][i];

    framebuffers[0][84+i]=dm_charset['#'][i];
    framebuffers[0][90+i]=dm_charset['H'][i];
    framebuffers[0][96+i]=dm_charset['S'][i];
    framebuffers[0][102+i]=dm_charset['B'][i];
    framebuffers[0][108+i]=dm_charset['X'][i];
    framebuffers[0][114+i]=dm_charset['L'][i];
//    framebuffers[0][120+i]=dm_charset[0x1][i];
//    framebuffers[0][126+i]=dm_charset[0x2][i];
  }
}

// Screen refresh routine in a timer interrupt, so a blocking loop() won't interrupt it.
void TimerInterruptCallback(void *pArg)
{
 const int rowselect[8] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x00 };
 int i, dispx;
 digitalWrite (PIN_OE, LOW); 
 for (dispx=0; dispx<LINELENGTH; dispx++)
 {
      if ( ((framebuffers[0][dispx] >> dispy) & 1) == 1 ) { digitalWrite (PIN_D, HIGH); } else { digitalWrite (PIN_D, LOW); }
      if ( ((framebuffers[1][dispx] >> dispy) & 1) == 1 ) { digitalWrite (PIN_D2, HIGH); } else { digitalWrite (PIN_D2, LOW); }
      digitalWrite (PIN_CP, HIGH);
      digitalWrite (PIN_CP, LOW);
 }
 for (i=0; i<8; i++)
 {
      if ( ((rowselect[dispy] >> i) & 1) == 1 ) { digitalWrite (PIN_D, LOW ); digitalWrite (PIN_D2, LOW ); } else { digitalWrite (PIN_D, HIGH); digitalWrite (PIN_D2, HIGH ); }
      digitalWrite (PIN_CP, HIGH);
      digitalWrite (PIN_CP, LOW);
 }
 digitalWrite (PIN_ST, LOW);
 digitalWrite (PIN_ST, HIGH);
 digitalWrite (PIN_OE, HIGH); 
 dispy++;
 if (dispy == 7) dispy=0;
}

void loop()
{
 static int i;
 Serial.print ("Hello from blocking loop ");
 Serial.println (i, HEX);
 i++;
 delay (1000);
}
